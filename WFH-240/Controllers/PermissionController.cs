﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFHDataModel;
using WFHRepository;
using WFHViewModel;

namespace WFH_240.Controllers
{
    public class PermissionController : Controller
    {
        // GET: Permission
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult HistoryList(int pageHis = 1, bool sortAscHis = true, int perPageHis = 10)
        {
            Pagination pg = PermissionRepo.PagingHis(pageHis, sortAscHis, perPageHis);
            ViewBag.currPageHis = pageHis;
            ViewBag.PageCountHis = pg.Pages;
            return PartialView("_HistoryList", pg.ListOf);
        }
        public ActionResult SearchHis(DateTime datefrom, DateTime dateto)
        {
            ViewBag.PageOf = 1;
            return PartialView("_HistoryList", PermissionRepo.DateSearchHis(datefrom, dateto));
        }

        public ActionResult ApprovalList(int pageApp = 1, bool sortAscApp = true, int perPageApp = 10)
        {
            Pagination pg = PermissionRepo.PagingApp(pageApp, sortAscApp, perPageApp);
            ViewBag.currPageApp = pageApp;
            ViewBag.PageCountApp = pg.Pages;
            return PartialView("_ApprovalList", pg.ListOf);
        }
        public ActionResult SearchApp(DateTime datefrom, DateTime dateto)
        {
            ViewBag.PageOf = 1;
            return PartialView("_ApprovalList", PermissionRepo.DateSearchApp(datefrom, dateto));
        }

        public ActionResult Approve(long id)
        {
            return PartialView("_Approve", PermissionRepo.ById(id));
        }
        [HttpPost]
        public ActionResult Approve(PermissionView model)
        {
            ResponseResult result = PermissionRepo.Approve(model, model.id);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult Reject(long id)
        {
            return PartialView("_Reject", PermissionRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Reject(PermissionView model)
        {
            ResponseResult result = PermissionRepo.Reject(model, model.id);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);
        }

        //Get
        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public ActionResult Create(PermissionView model)
        {
            ResponseResult result = PermissionRepo.Update(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                },
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail(long id)
        {
            return PartialView("_Detail", PermissionRepo.ById(id));
        }
    }
}