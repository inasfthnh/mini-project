﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFHDataModel;
using WFHRepository;
using WFHViewModel;

namespace WFH_240.Controllers
{
    public class ResourceProjectController : Controller
    {
        // GET: ResourceProject
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int page = 1, string search = "", bool sortAsc = true, int perPage = 10)
        {
            Pagination pg = ResourceProjectRepo.Paging(page, search, sortAsc, perPage);
            ViewBag.currPage = page;
            ViewBag.PageCount = pg.Pages;
            return PartialView("_List", pg.ListOf);
        }

        public ActionResult SearchDate(DateTime datefrom, DateTime dateto)
        {
            ViewBag.PageOf = 1;
            return PartialView("_List", ResourceProjectRepo.SearchDate(datefrom, dateto));
        }

        public ActionResult SearchBoth(DateTime datefrom, DateTime dateto, string search)
        {
            ViewBag.PageOf = 1;
            return PartialView("_List", ResourceProjectRepo.SearchBoth(datefrom, dateto, search));
        }

        public ActionResult Create()
        {
            ViewBag.ClientList = new SelectList(ClientRepo.All(), "id", "name");
            return PartialView("_Create");
        }

        [HttpPost]
        public ActionResult Create (ResourceProView model) 
        {
            ResponseResult result = ResourceProjectRepo.Create(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                },
                JsonRequestBehavior.AllowGet);
        }

        public ActionResult Detail(long id)
        {
            return PartialView("_Detail", ResourceProjectRepo.ById(id));
        }

        //Get
        public ActionResult Edit(long id)
        {
            ViewBag.ClientList = new SelectList(ClientRepo.All(), "id", "name");
            return PartialView("_Edit", ResourceProjectRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Edit(ResourceProView model)
        {
            ResponseResult result = ResourceProjectRepo.Edit(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                },
                JsonRequestBehavior.AllowGet);
        }

        //Get
        public ActionResult Delete(long id)
        {
            return PartialView("_Delete", ResourceProjectRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Delete(ResourceProView model)
        {
            ResponseResult result = ResourceProjectRepo.Delete(model.id);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);

        }
    }
}