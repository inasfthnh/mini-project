﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WFHDataModel;
using WFHRepository;

namespace WFH_240.Controllers
{
    public class ReligionController : Controller
    {
        // GET: Religion
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int page = 1, string search = "", bool sortAsc = true, int perPage = 10)
        {
            Pagination pg = ReligionRepo.Paging(page, search, sortAsc, perPage);
            ViewBag.currPage = page;
            ViewBag.PageCount = pg.Pages;
            return PartialView("_List", pg.ListOf);
        }

        //Get
        public ActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public ActionResult Create(x_religion model)
        {
            ResponseResult result = ReligionRepo.Update(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    entity = result.Entity
                },
                JsonRequestBehavior.AllowGet);
        }

        //Get
        public ActionResult Edit(long id)
        {
            return PartialView("_Edit", ReligionRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Edit(x_religion model)
        {
            ResponseResult result = ReligionRepo.Update(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);
        }

        //Get
        public ActionResult Delete(long id)
        {
            return PartialView("_Delete", ReligionRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Delete(x_religion model)
        {
            ResponseResult result = ReligionRepo.Delete(model.id);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message
                },
                JsonRequestBehavior.AllowGet);

        }

    }
}