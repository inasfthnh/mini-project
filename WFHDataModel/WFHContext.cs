namespace WFHDataModel
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class WFHContext : DbContext
    {
        public WFHContext()
            : base("name=WFHContext")
        {
        }

        public virtual DbSet<x_permission> x_permission { get; set; }
        public virtual DbSet<x_religion> x_religion { get; set; }
        public virtual DbSet<x_resource_project> x_resource_project { get; set; }
        public virtual DbSet<x_client> x_client { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<x_permission>()
                .Property(e => e.employee_name)
                .IsUnicode(false);

            modelBuilder.Entity<x_permission>()
                .Property(e => e.department)
                .IsUnicode(false);

            modelBuilder.Entity<x_permission>()
                .Property(e => e.position)
                .IsUnicode(false);

            modelBuilder.Entity<x_permission>()
                .Property(e => e.notes)
                .IsUnicode(false);

            modelBuilder.Entity<x_permission>()
                .Property(e => e.status)
                .IsUnicode(false);

            modelBuilder.Entity<x_religion>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<x_religion>()
                .Property(e => e.description)
                .IsUnicode(false);

            modelBuilder.Entity<x_resource_project>()
                .Property(e => e.location)
                .IsUnicode(false);

            modelBuilder.Entity<x_resource_project>()
                .Property(e => e.department)
                .IsUnicode(false);

            modelBuilder.Entity<x_resource_project>()
                .Property(e => e.pic_name)
                .IsUnicode(false);

            modelBuilder.Entity<x_resource_project>()
                .Property(e => e.project_name)
                .IsUnicode(false);

            modelBuilder.Entity<x_resource_project>()
                .Property(e => e.project_role)
                .IsUnicode(false);

            modelBuilder.Entity<x_resource_project>()
                .Property(e => e.project_phase)
                .IsUnicode(false);

            modelBuilder.Entity<x_resource_project>()
                .Property(e => e.project_description)
                .IsUnicode(false);

            modelBuilder.Entity<x_resource_project>()
                .Property(e => e.project_technology)
                .IsUnicode(false);

            modelBuilder.Entity<x_resource_project>()
                .Property(e => e.main_task)
                .IsUnicode(false);

            modelBuilder.Entity<x_client>()
                .Property(e => e.name)
                .IsUnicode(false);

            modelBuilder.Entity<x_client>()
                .Property(e => e.user_client_name)
                .IsUnicode(false);

            modelBuilder.Entity<x_client>()
                .Property(e => e.user_email)
                .IsUnicode(false);
        }
    }
}
