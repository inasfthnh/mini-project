namespace WFHDataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class x_permission
    {
        public long id { get; set; }

        public long created_by { get; set; }

        public DateTime created_on { get; set; }

        public long? modified_by { get; set; }

        public DateTime? modified_on { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_on { get; set; }

        public bool is_delete { get; set; }

        [Required]
        [StringLength(255)]
        public string employee_name { get; set; }

        [StringLength(100)]
        public string department { get; set; }

        [StringLength(100)]
        public string position { get; set; }

        public DateTime permission_time { get; set; }

        [Required]
        [StringLength(255)]
        public string notes { get; set; }

        public bool is_absent { get; set; }

        public bool is_sick { get; set; }

        public bool is_coming_late { get; set; }

        public bool is_early_leave { get; set; }

        public bool others { get; set; }

        [Required]
        [StringLength(20)]
        public string status { get; set; }
    }
}
