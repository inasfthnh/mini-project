namespace WFHDataModel
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class x_resource_project
    {
        public long id { get; set; }

        public long created_by { get; set; }

        public DateTime created_on { get; set; }

        public long? modified_by { get; set; }

        public DateTime? modified_on { get; set; }

        public long? deleted_by { get; set; }

        public DateTime? deleted_on { get; set; }

        public bool is_delete { get; set; }

        public long client_id { get; set; }

        [StringLength(100)]
        public string location { get; set; }

        [StringLength(100)]
        public string department { get; set; }

        [StringLength(100)]
        public string pic_name { get; set; }

        [Required]
        [StringLength(100)]
        public string project_name { get; set; }

        [Required, DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime start_project { get; set; }

        [Required, DataType(DataType.Date), DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime end_project { get; set; }

        [Required]
        [StringLength(255)]
        public string project_role { get; set; }

        [Required]
        [StringLength(255)]
        public string project_phase { get; set; }

        [Required]
        [StringLength(255)]
        public string project_description { get; set; }

        [Required]
        [StringLength(255)]
        public string project_technology { get; set; }

        [Required]
        [StringLength(255)]
        public string main_task { get; set; }

    }
}
