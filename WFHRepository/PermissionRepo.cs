﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFHDataModel;
using WFHViewModel;

namespace WFHRepository
{
    public class PermissionRepo
    {
        //List
        public static List<PermissionView> All()
        {
            List<PermissionView> result = new List<PermissionView>();
            using (var db = new WFHContext())
            {
                result = (from p in db.x_permission
                          select new PermissionView
                          {
                              id = p.id,
                              employee_name = p.employee_name,
                              department = p.department,
                              position = p.position,
                              permission_time = p.permission_time,
                              notes = p.notes,
                              is_absent = p.is_absent,
                              is_sick = p.is_sick,
                              is_coming_late = p.is_coming_late,
                              is_early_leave = p.is_early_leave,
                              others = p.others,
                              status = p.status
                          }).ToList();
            }
            return result;
        }

        public static Pagination PagingHis(int pageHis, bool sortAscHis, int perPageHis)
        {
            Pagination result = new Pagination();
            using (var db = new WFHContext())
            {
                int rowCntHis = db.x_permission.Count(p => p.is_delete == false);
                int pageCntHis = rowCntHis / perPageHis;
                if (rowCntHis % perPageHis != 0)
                {
                    pageCntHis++;
                }

                var query = (from p in db.x_permission
                             where (p.is_delete == false)
                             select new PermissionView
                             {
                                 id = p.id,
                                 created_by = p.created_by,
                                 created_on = p.created_on,
                                 employee_name = p.employee_name,
                                 department = p.department,
                                 position = p.position,
                                 permission_time = p.permission_time,
                                 notes = p.notes,
                                 is_absent = p.is_absent,
                                 is_sick = p.is_sick,
                                 is_coming_late = p.is_coming_late,
                                 is_early_leave = p.is_early_leave,
                                 others = p.others,
                                 status = p.status
                             });

                if (sortAscHis)
                {
                    query = query.OrderBy(p => p.permission_time);
                }
                else
                {
                    query = query.OrderByDescending(p => p.permission_time);
                }

                query = query.Skip((pageHis - 1) * perPageHis).Take(perPageHis);

                result.ListOf = query.ToList();
                result.Pages = pageCntHis;
            }
            return result;
        }

        public static List<PermissionView> DateSearchHis(DateTime datefrom, DateTime dateto)
        {
            List<PermissionView> result = new List<PermissionView>();
            using (var db = new WFHContext())
            {
                string[] todate = dateto.ToString().Split(' ');
                string totanggal = string.Format("{0}", todate[0]);
                string towaktu = "23:59:59.000";
                DateTime ketanggal = DateTime.Parse(string.Format("{0} {1}", totanggal, towaktu));
                result = (from p in db.x_permission
                          where (p.permission_time >= datefrom && p.permission_time <= ketanggal && p.is_delete == false)
                          orderby p.permission_time
                          select new PermissionView
                          {
                              id = p.id,
                              employee_name = p.employee_name,
                              department = p.department,
                              position = p.position,
                              permission_time = p.permission_time,
                              notes = p.notes,
                              is_absent = p.is_absent,
                              is_sick = p.is_sick,
                              is_coming_late = p.is_coming_late,
                              is_early_leave = p.is_early_leave,
                              others = p.others,
                              status = p.status
                          }).ToList();
            }
            return result;
        }

        public static Pagination PagingApp(int pageApp, bool sortAscApp, int perPageApp)
        {
            Pagination result = new Pagination();
            using (var db = new WFHContext())
            {
                int rowCntApp = db.x_permission.Count(p => p.status == "Submitted" && p.is_delete == false);
                int pageCntApp = rowCntApp / perPageApp;
                if (rowCntApp % perPageApp != 0)
                {
                    pageCntApp++;
                }

                var query = (from p in db.x_permission
                             where (p.status == "Submitted" && p.is_delete == false)
                             select new PermissionView
                             {
                                 id = p.id,
                                 created_by = p.created_by,
                                 created_on = p.created_on,
                                 employee_name = p.employee_name,
                                 department = p.department,
                                 position = p.position,
                                 permission_time = p.permission_time,
                                 notes = p.notes,
                                 is_absent = p.is_absent,
                                 is_sick = p.is_sick,
                                 is_coming_late = p.is_coming_late,
                                 is_early_leave = p.is_early_leave,
                                 others = p.others,
                                 status = p.status
                             });

                //.Where(o => o.permission_time >= datefrom && o.permission_time <= dateto
                if (sortAscApp)
                {
                    query = query.OrderBy(o => o.permission_time);
                }
                else
                {
                    query = query.OrderByDescending(o => o.permission_time);
                }

                query = query.Skip((pageApp - 1) * perPageApp).Take(perPageApp);

                result.ListOf = query.ToList();
                result.Pages = pageCntApp;
            }
            return result;
        }

        public static List<PermissionView> DateSearchApp(DateTime datefrom, DateTime dateto)
        {
            List<PermissionView> result = new List<PermissionView>();
            using (var db = new WFHContext())
            {
                string[] todate = dateto.ToString().Split(' ');
                string totanggal = string.Format("{0}", todate[0]);
                string towaktu = "23:59:59.000";
                DateTime ketanggal = DateTime.Parse(string.Format("{0} {1}", totanggal, towaktu));
                result = (from p in db.x_permission
                          where (p.permission_time >= datefrom && p.permission_time <= ketanggal && p.is_delete == false && p.status == "Submitted")
                          orderby p.permission_time
                          select new PermissionView
                          {
                              id = p.id,
                              employee_name = p.employee_name,
                              department = p.department,
                              position = p.position,
                              permission_time = p.permission_time,
                              notes = p.notes,
                              is_absent = p.is_absent,
                              is_sick = p.is_sick,
                              is_coming_late = p.is_coming_late,
                              is_early_leave = p.is_early_leave,
                              others = p.others,
                              status = p.status
                          }).ToList();
            }
            return result;
        }
    
        //Create
        public static ResponseResult Update(PermissionView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new WFHContext())
                {
                    if (entity.id == 0)
                    {
                        x_permission permission = new x_permission();
                        if (entity.tanggal != null)
                        {
                            string[] date = entity.tanggal.Split(' ');
                            entity.tanggal = string.Format("{0}", date[0]);
                            if (entity.time == null)
                                { entity.time = "00:00:00.000"; }
                            else
                                { entity.time = string.Format(entity.time + ":00.000"); }
                            entity.permission_time = DateTime.Parse(string.Format("{0} {1}", entity.tanggal, entity.time));
                        }
                        
                        // Create
                        entity.created_by = 0;
                        entity.created_on = DateTime.Now;
                        permission.created_by = entity.created_by;
                        permission.created_on = entity.created_on;
                        permission.employee_name = entity.employee_name;
                        permission.department = entity.department;
                        permission.position = entity.position;
                        permission.permission_time = entity.permission_time;
                        permission.notes = entity.notes;
                        permission.is_absent = entity.is_absent;
                        permission.is_sick = entity.is_sick;
                        permission.is_coming_late = entity.is_coming_late;
                        permission.is_early_leave = entity.is_early_leave;
                        permission.others = entity.others;
                        entity.status = "Submitted";
                        permission.status = entity.status;

                        if (result.Success == true)
                        {
                            db.x_permission.Add(permission);
                            db.SaveChanges();
                            result.Message = "Berhasil dibuat";
                            result.Entity = entity;
                        }                      
                    }
                    /*else
                    {
                        //Edit
                        x_permission permissioned = db.x_permission
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                        if (permissioned == null)
                        {
                            result.Success = false;
                        }
                        else
                        {
                            permissioned.status = statusSub;
                            permissioned.modified_by = 0;
                            permissioned.modified_on = DateTime.Now;
                            db.SaveChanges();

                            result.Message = "Berhasil diubah";
                            result.Entity = entity;
                        }
                    }*/
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static PermissionView ById(long id)
        {
            PermissionView result = new PermissionView();
            try
            {
                using (var db = new WFHContext())
                {
                    result = (from p in db.x_permission
                              where (p.id == id)
                              select new PermissionView
                              {
                                  id = p.id,
                                  employee_name = p.employee_name,
                                  department = p.department,
                                  position = p.position,
                                  permission_time = p.permission_time,
                                  notes = p.notes,
                                  is_absent = p.is_absent,
                                  is_sick = p.is_sick,
                                  is_coming_late = p.is_coming_late,
                                  is_early_leave = p.is_early_leave,
                                  others = p.others,
                                  status = p.status
                              }).FirstOrDefault();

                    /*string[] dateinput = result.permission_time.ToString().Split('/');
                    string[] dateoutput = dateinput[2].Split(' ');
                    string[] dateoutput1 = dateoutput[1].Split(':');
                    dateinput[2] = dateoutput[0];

                    switch (int.Parse(dateinput[0]))
                    {
                        case 1:
                            dateinput[0] = ("Januari");
                            break;
                        case 2:
                            dateinput[0] = ("Februari");
                            break;
                        case 3:
                            dateinput[0] = ("Maret");
                            break;
                        case 4:
                            dateinput[0] = ("April");
                            break;
                        case 5:
                            dateinput[0] = ("Mei");
                            break;
                        case 6:
                            dateinput[0] = ("Juni");
                            break;
                        case 7:
                            dateinput[0] = ("Juli");
                            break;
                        case 8:
                            dateinput[0] = ("Agustus");
                            break;
                        case 9:
                            dateinput[0] = ("September");
                            break;
                        case 10:
                            dateinput[0] = ("Oktober");
                            break;
                        case 11:
                            dateinput[0] = ("November");
                            break;
                        case 12:
                            dateinput[0] = ("Desember");
                            break;
                        default:
                            dateinput[0] = (" ");
                            break;
                    }
                    if (int.Parse(dateoutput1[0]) > 12)
                    {
                        dateoutput1[0] = dateoutput1[0] + 12;
                    }
                    result.newdatetime = string.Format("{0} {1}, {2} - {3}:{4}", dateinput[0], dateinput[2], dateinput[1], dateoutput1[0], dateoutput1[1]);
                    result.newdate = string.Format("{0} {1}, {2}", dateinput[0], dateinput[1], dateinput[2]); */
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return result == null ? new PermissionView() : result;
        }

        public static ResponseResult Approve(PermissionView model, long id)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new WFHContext())
                {
                    x_permission permission = db.x_permission
                        .Where(o => o.id == id)
                        .FirstOrDefault();

                    x_permission oldPermission = permission;
                    result.Entity = oldPermission;

                    if (permission != null)
                    {
                        permission.status = "Approved";
                        permission.modified_by = 0;
                        permission.modified_on = DateTime.Now;
                        model.status = permission.status;
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Permission not found";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }

        public static ResponseResult Reject(PermissionView model, long id)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new WFHContext())
                {
                    x_permission permission = db.x_permission
                        .Where(o => o.id == id)
                        .FirstOrDefault();

                    x_permission oldPermission = permission;
                    result.Entity = oldPermission;

                    if (permission != null)
                    {
                        permission.status = "Rejected";
                        permission.modified_by = 0;
                        permission.modified_on = DateTime.Now;
                        model.status = permission.status;
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Category not found";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
