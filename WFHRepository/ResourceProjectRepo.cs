﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFHDataModel;
using WFHViewModel;

namespace WFHRepository
{
    public class ResourceProjectRepo
    {
        public static List<ResourceProView> All()
        {
            List<ResourceProView> result = new List<ResourceProView>();
            using (var db = new WFHContext())
            {
                result = (from r in db.x_resource_project
                          join c in db.x_client
                          on r.client_id equals c.id
                          where (r.is_delete == false)
                             select new ResourceProView
                             {
                                 id = r.id,
                                 created_by = r.created_by,
                                 created_on = r.created_on,
                                 client_id = r.client_id,
                                 location = r.location,
                                 department = r.department,
                                 pic_name = r.pic_name,
                                 project_name = r.project_name,
                                 start_project = r.start_project,
                                 end_project = r.end_project,
                                 project_role = r.project_role,
                                 project_phase = r.project_phase,
                                 project_description = r.project_description,
                                 project_technology = r.project_technology,
                                 main_task = r.main_task,
                                 clientname = c.name
                             }).ToList();
            }
            return result;
        }

        public static Pagination Paging(int page, string search, bool sortAsc, int perPage)
        {
            Pagination result = new Pagination();
            using (var db = new WFHContext())
            {
                int rowCnt = 1;
                if (search != "")
                {
                    rowCnt = db.x_resource_project.Count(o => o.is_delete == false) - db.x_client.Count(o => !o.name.Contains(search) && o.is_delete == false) - 1;  
                } else {
                    rowCnt = db.x_resource_project.Count(o => o.is_delete == false);
                }

                int pageCnt = rowCnt / perPage;
                if (rowCnt % perPage != 0)
                {
                    pageCnt++;
                }


                var query = (from r in db.x_resource_project
                             join c in db.x_client
                             on r.client_id equals c.id
                             where (r.is_delete == false && c.name.Contains(search))
                             select new ResourceProView
                             {
                                 id = r.id,
                                 created_by = r.created_by,
                                 created_on = r.created_on,
                                 client_id = r.client_id,
                                 location = r.location,
                                 department = r.department,
                                 pic_name = r.pic_name,
                                 project_name = r.project_name,
                                 start_project = r.start_project,
                                 end_project = r.end_project,
                                 project_role = r.project_role,
                                 project_phase = r.project_phase,
                                 project_description = r.project_description,
                                 project_technology = r.project_technology,
                                 main_task = r.main_task,
                                 clientname = c.name
                             });

                if (sortAsc)
                {
                    query = query.OrderBy(o => o.clientname);
                }
                else
                {
                    query = query.OrderByDescending(o => o.clientname);
                }

                query = query.Skip((page - 1) * perPage).Take(perPage);

                result.ListOf = query.ToList();
                result.Pages = pageCnt;
            }
            return result;
        }

        public static List<ResourceProView> SearchDate(DateTime datefrom, DateTime dateto)
        {
            List<ResourceProView> result = new List<ResourceProView>();
            using (var db = new WFHContext())
            {
                result = (from r in db.x_resource_project
                          join c in db.x_client
                          on r.client_id equals c.id
                          where (
                          ((r.start_project <= datefrom && r.end_project >= datefrom) ||
                           (r.start_project >= datefrom && r.end_project <= dateto) ||
                           (r.start_project <= dateto && r.end_project >= dateto)) 
                           && r.is_delete == false)
                           orderby c.name
                          select new ResourceProView
                          {
                              id = r.id,
                              created_by = r.created_by,
                              created_on = r.created_on,
                              client_id = r.client_id,
                              location = r.location,
                              department = r.department,
                              pic_name = r.pic_name,
                              project_name = r.project_name,
                              start_project = r.start_project,
                              end_project = r.end_project,
                              project_role = r.project_role,
                              project_phase = r.project_phase,
                              project_description = r.project_description,
                              project_technology = r.project_technology,
                              main_task = r.main_task,
                              clientname = c.name
                          }).ToList();
            }
            return result;
        }

        public static List<ResourceProView> SearchBoth(DateTime datefrom, DateTime dateto, string search)
        {
            List<ResourceProView> result = new List<ResourceProView>();
            using (var db = new WFHContext())
            {
                result = (from r in db.x_resource_project
                          join c in db.x_client
                          on r.client_id equals c.id
                          where (
                          ((r.start_project <= datefrom && r.end_project >= datefrom) ||
                           (r.start_project >= datefrom && r.end_project <= dateto) ||
                           (r.start_project <= dateto && r.end_project >= dateto))
                           && r.is_delete == false && c.name.Contains(search))
                          orderby c.name
                          select new ResourceProView
                          {
                              id = r.id,
                              created_by = r.created_by,
                              created_on = r.created_on,
                              client_id = r.client_id,
                              location = r.location,
                              department = r.department,
                              pic_name = r.pic_name,
                              project_name = r.project_name,
                              start_project = r.start_project,
                              end_project = r.end_project,
                              project_role = r.project_role,
                              project_phase = r.project_phase,
                              project_description = r.project_description,
                              project_technology = r.project_technology,
                              main_task = r.main_task,
                              clientname = c.name
                          }).ToList();
            }
            return result;
        }      
      
        //Create
        public static ResponseResult Create(ResourceProView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new WFHContext())
                {                   
                        x_resource_project resourcepro = new x_resource_project();
                        if (entity.startpro != null && entity.endpro != null)
                        {
                            entity.start_project = DateTime.Parse(entity.startpro);
                            entity.end_project = DateTime.Parse(entity.endpro);
                        }
                        // Create
                        entity.created_by = 0;
                        entity.created_on = DateTime.Now;
                        resourcepro.created_by = entity.created_by;
                        resourcepro.created_on = entity.created_on;
                        resourcepro.client_id = entity.client_id;
                        resourcepro.location = entity.location;
                        resourcepro.department = entity.department;
                        resourcepro.pic_name = entity.pic_name;
                        resourcepro.project_name = entity.project_name;
                        resourcepro.start_project = entity.start_project;
                        resourcepro.end_project = entity.end_project;
                        resourcepro.project_role = entity.project_role;
                        resourcepro.project_phase = entity.project_phase;
                        resourcepro.project_description = entity.project_description;
                        resourcepro.project_technology = entity.project_technology;
                        resourcepro.main_task = entity.main_task;
                        resourcepro.is_delete = false;

                        if (entity.start_project > entity.end_project || entity.startpro == null || entity.endpro == null)
                        { result.Success = false; }

                        if (result.Success == true)
                        {
                            db.x_resource_project.Add(resourcepro);
                            db.SaveChanges();
                            result.Message = "Berhasil dibuat";
                            result.Entity = entity;
                        }
                    
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static ResponseResult Edit(ResourceProView entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new WFHContext())
                {
                    
                        x_resource_project resourcepro = db.x_resource_project
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                        if (resourcepro != null)
                        {
                            resourcepro.client_id = entity.client_id;
                            resourcepro.location = entity.location;
                            resourcepro.department = entity.department;
                            resourcepro.pic_name = entity.pic_name;
                            resourcepro.project_name = entity.project_name;
                            resourcepro.start_project = entity.start_project;
                            resourcepro.end_project = entity.end_project;
                            resourcepro.project_role = entity.project_role;
                            resourcepro.project_phase = entity.project_phase;
                            resourcepro.project_description = entity.project_description;
                            resourcepro.project_technology = entity.project_technology;
                            resourcepro.main_task = entity.main_task;

                            if (entity.start_project > entity.end_project || entity.start_project == default || entity.end_project == default)
                            { result.Success = false; }

                            if (result.Success == true)
                            {
                                resourcepro.modified_by = 0;
                                resourcepro.modified_on = DateTime.Now;
                                db.SaveChanges();
                                result.Message = "Berhasil diubah";
                                result.Entity = entity;
                            }
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Resource Project tidak ditemukan";
                        }
                    
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static ResourceProView ById(long id)
        {
            ResourceProView result = new ResourceProView();
            try
            {
                using (var db = new WFHContext())
                {
                    result = (from r in db.x_resource_project
                              join c in db.x_client
                              on r.client_id equals c.id
                              where (r.id == id)
                              select new ResourceProView
                              {
                                  id = r.id,
                                  created_by = r.created_by,
                                  created_on = r.created_on,
                                  client_id = r.client_id,
                                  location = r.location,
                                  department = r.department,
                                  pic_name = r.pic_name,
                                  project_name = r.project_name,
                                  start_project = r.start_project,
                                  end_project = r.end_project,
                                  project_role = r.project_role,
                                  project_phase = r.project_phase,
                                  project_description = r.project_description,
                                  project_technology = r.project_technology,
                                  main_task = r.main_task,
                                  clientname = c.name
                              }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return result == null ? new ResourceProView() : result;
        }

        public static ResponseResult Delete(long id)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new WFHContext())
                {
                    x_resource_project resourcepro = db.x_resource_project
                        .Where(o => o.id == id)
                        .FirstOrDefault();

                    x_resource_project oldresourcepro = resourcepro;
                    result.Entity = oldresourcepro;

                    if (resourcepro != null)
                    {
                        resourcepro.deleted_by = 0;
                        resourcepro.deleted_on = DateTime.Now;
                        resourcepro.is_delete = true;

                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Resource Project tidak ditemukan";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}
