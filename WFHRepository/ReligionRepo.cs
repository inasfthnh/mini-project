﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFHDataModel;

namespace WFHRepository
{
    public class ReligionRepo
    {
        //List
        public static List<x_religion> All()
        {
            List<x_religion> result = new List<x_religion>();
            using (var db = new WFHContext())
            {
                result = db.x_religion
                    .ToList();
            }
            return result;
        }

        public static Pagination Paging(int page, string search, bool sortAsc, int perPage)
        {
            Pagination result = new Pagination();
            using (var db = new WFHContext())
            {
                int rowCnt = db.x_religion.Count(o => o.name.Contains(search) && o.is_delete == false);
                int pageCnt = rowCnt / perPage;
                if (rowCnt % perPage != 0)
                {
                    pageCnt++;
                }

                var query = db.x_religion
                    .Where(o => o.name.Contains(search) && o.is_delete == false);

                if (sortAsc)
                {
                    query = query.OrderBy(o => o.name);
                }
                else
                {
                    query = query.OrderByDescending(o => o.name);
                }

                query = query.Skip((page - 1) * perPage).Take(perPage);
               
                result.ListOf = query.ToList();
                result.Pages = pageCnt;
            }
            return result;
        }

        //Create
        public static ResponseResult Update(x_religion entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new WFHContext())
                {
                    if (entity.id == 0)
                    {
                        // Create
                        entity.created_by = 0;
                        entity.created_on = DateTime.Now;
                        if (entity.description == null)
                        {
                            entity.description = "-";
                        }
                        db.x_religion.Add(entity);
                        db.SaveChanges();

                        result.Message = "Berhasil dibuat";
                        result.Entity = entity;
                    }
                    else
                    {
                        //Edit
                        x_religion religion = db.x_religion
                            .Where(o => o.id == entity.id)
                            .FirstOrDefault();
                        if (religion == null)
                        {
                            result.Success = false;
                        }
                        else
                        {
                            religion.name = entity.name;
                            religion.description = entity.description;
                            religion.modified_by = 0;
                            religion.modified_on = DateTime.Now;
                            db.SaveChanges();

                            result.Message = "Berhasil diubah";
                            result.Entity = entity;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }
            return result;
        }

        public static x_religion ById(long id)
        {
            x_religion result = new x_religion();
            try
            {
                using (var db = new WFHContext())
                {
                    result = db.x_religion
                        .Where(o => o.id == id)
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return result == null ? new x_religion() : result;
        }

        public static ResponseResult Delete(long id)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new WFHContext())
                {
                    x_religion religion = db.x_religion
                        .Where(o => o.id == id)
                        .FirstOrDefault();

                    x_religion oldReligion = religion;
                    result.Entity = oldReligion;

                    if (religion != null)
                    {
                        religion.deleted_by = 0;
                        religion.deleted_on = DateTime.Now;
                        religion.is_delete = true;

                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Agama tidak ditemukan";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }
}