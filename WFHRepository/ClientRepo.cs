﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFHDataModel;

namespace WFHRepository
{
    public class ClientRepo
    {
        public static List<x_client> All()
        {
            List<x_client> result = new List<x_client>();
            using (var db = new WFHContext())
            {
                result = db.x_client.Where(o => o.is_delete == false).ToList();
            }
            return result;
        }
    }
}
