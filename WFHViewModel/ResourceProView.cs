﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace WFHViewModel
{
    public class ResourceProView
    {
        public long id { get; set; }

        public long created_by { get; set; }

        public DateTime created_on { get; set; }

        [Required(ErrorMessage = "Kolom Client harus diisi."), Display(Name = "Client")]
        public long client_id { get; set; }

        [StringLength(100)]
        public string location { get; set; }

        [StringLength(100)]
        public string department { get; set; }

        [StringLength(100)]
        public string pic_name { get; set; }

        [Required(ErrorMessage = "Kolom Project Name harus diisi.")]
        [StringLength(100), Display(Name = "Project Name")]
        public string project_name { get; set; }

        [DataType(DataType.Date), Required(ErrorMessage = "Kolom Start Project harus diisi.")]
        [Display(Name = "Start Project"), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime start_project { get; set; }
               
        [DataType(DataType.Date), Required(ErrorMessage = "Kolom End Project harus diisi.")]
        [Display(Name = "End Project"), DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime end_project { get; set; }

        [Required(ErrorMessage = "Kolom Project Role harus diisi.")]
        [StringLength(255), Display(Name = "Project Role")]
        public string project_role { get; set; }

        [Required(ErrorMessage = "Kolom Project Phase harus diisi.")]
        [StringLength(255), Display(Name = "Project Phase")]
        public string project_phase { get; set; }

        [Required(ErrorMessage = "Kolom Project Description harus diisi.")]
        [StringLength(255), Display(Name = "Project Description")]
        public string project_description { get; set; }

        [Required(ErrorMessage = "Kolom Project Technology harus diisi.")]
        [StringLength(255), Display(Name = "Project Technology")]
        public string project_technology { get; set; }

        [Required(ErrorMessage = "Kolom Main Task harus diisi.")]
        [StringLength(255), Display(Name = "Main Task")]
        public string main_task { get; set; }

        public string startnewdate
        {
            get { return start_project.ToString("MMMM d, yyyy"); }
        }

        public string endnewdate
        {
            get { return end_project.ToString("MMMM d, yyyy"); }
        }

        [Required(ErrorMessage = "Kolom Start Project harus diisi."), Display(Name = "Start Project")]
        public string startpro { get; set; }
        [Required(ErrorMessage = "Kolom End Project harus diisi."), Display(Name = "End Project")]
        public string endpro { get; set; }

        public string clientname { get; set; }
    }
}
