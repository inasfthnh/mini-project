﻿    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

namespace WFHViewModel
{
    public class PermissionView
    {
        public long id { get; set; }
        public long created_by { get; set; }
        public DateTime created_on { get; set; }

        [Required(ErrorMessage = "Kolom Nama harus diisi.")]
        [StringLength(255), Display(Name = "Nama")]
        public string employee_name { get; set; }

        [StringLength(100), Display(Name = "Departemen / Divisi")]
        public string department { get; set; }

        [StringLength(100), Display(Name = "Jabatan")]
        public string position { get; set; }

        [Required(ErrorMessage = "Kolom Tanggal harus diisi.")]
        [Display(Name = "Tanggal")]
        public string tanggal { get; set; }

        public string time { get; set; }

        public DateTime permission_time { get; set; }

        [Required(ErrorMessage = "Kolom Keterangan harus diisi.")]
        [StringLength(255), Display(Name = "Keterangan")]
        public string notes { get; set; }

        [Display(Name = "Tidak Masuk")]
        public bool is_absent { get; set; }

        [Display(Name = "Sakit")]
        public bool is_sick { get; set; }

        [Display(Name = "Datang Terlambat")]
        public bool is_coming_late { get; set; }

        [Display(Name = "Pulang Awal")]
        public bool is_early_leave { get; set; }

        [Display(Name = "Lainnya")]
        public bool others { get; set; }

        [Required]
        [StringLength(20)]
        public string status { get; set; }
        
        public string absent {
            get
            {
                if (is_absent && (is_sick || is_coming_late || is_early_leave || others))
                { return "Tidak Masuk,"; }
                else if (is_absent)
                { return "Tidak Masuk"; }
                else { return ""; }
            }
        }
        public string sick { 
            get 
            {   if (is_sick && (is_coming_late || is_early_leave || others))
                { return "Sakit,"; }
                else if (is_sick) 
                { return "Sakit"; }              
                else { return ""; }
            } 
        }
        public string cominglate {
            get
            {   if (is_coming_late && (is_early_leave || others))
                { return "Datang Terlambat,"; }
                else if (is_coming_late)
                { return "Datang Terlambat"; }
                else { return ""; }
            }
        }
        public string earlyleave {
            get
            {   if (is_early_leave && others)
                { return "Pulang Awal,"; }
                else if (is_early_leave)
                { return "Pulang Awal"; }   
                else { return ""; }
            }
        }
        public string other{
            get
            {   if (others)
                { return "Lainnya"; }
                else { return ""; }
            }
        }

        public string newdate {
            get { return permission_time.ToString("MMMM d, yyyy"); }
            //set; 
        }

        public string newdatetime {
            get {   if (permission_time.ToString("HH:mm:ss") == "00:00:00")
                    { return permission_time.ToString("MMMM yyyy, d -"); }
                    else { return permission_time.ToString("MMMM yyyy, d - HH:mm"); } 
            }
        }
    }
}
